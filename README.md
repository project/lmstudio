# Drupal LLM Provider for the LM Studio API

## Overview

This module serves as an LLM Provider for [LM Studio](https://lmstudio.ai/),
a platform that facilitates the local downloading and running
of Large Language Models (LLMs) while ensuring seamless integration with
[Hugging Face](https://huggingface.co/). LM Studio provides an out-of-the-box
API that this Drupal module can interact with. As a result, you can now easily
test any LLM compatible with LM Studio directly within your Drupal site,
before committing to development of a custom API.

## Usage

To use the Drupal LLM Provider for the LM Studio API module,
enable the module as you would  with any other Drupal module.

Dependency of this module is the
[LLM Provider Service](https://www.drupal.org/project/llm_provider) module,
which contains the  LLM Chat Example submodule - enable it to chat
with your LLM from Drupal.

## Requirements

- Drupal 9 or 10
- [LLM Provider Service](https://www.drupal.org/project/llm_provider) module.

## Installation

1. Install as you would normally install a contributed Drupal module. Visit:
   [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules)
   for further information.
2. Enable the module through the Drupal administration interface or via Drush
   (`drush en lmstudio`).

## Support & Maintenance

For any issues or feature requests, please use the
[issue tracker](https://www.drupal.org/project/issues/lmstudio) on
the project page.

## Maintainer

Michal Gow (seogow) - https://www.drupal.org/u/seogow
