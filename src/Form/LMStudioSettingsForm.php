<?php

namespace Drupal\lmstudio\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form to set the API Url and port of LM Studio.
 */
class LMStudioSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['lmstudio.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'lmstudio_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('lmstudio.settings');

    $form['lmstudio_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('LMStudio Domain/IP'),
      '#default_value' => $config->get('domain'),
      '#description' => $this->t('Include http/https prefix.'),
      '#required' => TRUE,
    ];

    $form['lmstudio_port'] = [
      '#type' => 'textfield',
      '#title' => $this->t('LMStudio Port'),
      '#default_value' => $config->get('port'),
      '#description' => $this->t('Provide port number if different from 80 (for http) or 443 (for https).'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('lmstudio.settings')
      ->set('domain', $form_state->getValue('lmstudio_domain'))
      ->set('port', $form_state->getValue('lmstudio_port'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
